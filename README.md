# README #

This project uses Bootstrap4, Jquery3.3.* and writes SCSS.

### How to compile scss to css? ###

* At first, you must install nodejs in your computer
* Go to the 'node' directory to see package.json
* If you have never installed the package for node-sass, you can install it with "npm install"
* Run the nodejs command with "npm run scss" for watching & compiling scss to css with node-sass
