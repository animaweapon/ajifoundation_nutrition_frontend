<!doctype html>
<html lang="en">
    <?php $title_page = 'Elerning'; include 'layout/head.php' ?>
    <body>
        <?php include 'layout/header.php' ?>
        <?php 
			$breadcrumbs = [
				['title' => 'หน้าแรก', 'url' => 'home.php'],
				['title' => 'อีเลิร์นนิ่ง']
			]; 
			include 'layout/breadcrumb.php'
		?>
        <div id="elearning">
            <div class="banner">
                <div class="container">
                    <div class="row pt-5">
                        <div class="col-12 col-sm-6">
                            <h1>อีเลิร์นนิ่ง</h1>
                            <p>การศึกษา ถือเป็นรากฐานที่สำคัญยิ่งของการพัฒนาประเทศ
                                    และเป็นแรงขับเคลื่อนที่สำคัญในการปูพื้นฐานให้มั่นคงพร้อมที่
                                    จะนำพาประเทศชาติของเรา ..</p>
                        </div>
                        <div class="col-12 col-sm-6 img">
                            <img src="images/elearning/bg.png" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="activity float-full-width">
                <div class="container">
                    <div class="text-header">
                        <span></span><h1>คอร์สล่าสุด</h1>
                    </div>
                    <div class="block-activity">
                        <?php for($i=0;$i<5;$i++){ ?>
                        <div class="activity-content">
                            <div class="row getArea">
                                <div class="col-4"><img src="images/home/activity/activity-1.png" class="img-fluid"></div>
                                <div class="col-8">
                                    <h3>โภชนาการของเด็กโต</h3>
                                    <h5>คอร์สทั่วไป บทที่ 2/8</h5>
                                    <p>เรื่อง : หลักการคำนวณโภชนาการของเด็กโต 5 - 10 ขวบ</p>
                                </div>
                            </div>
                            <div class="row hashtag">
                                <div class="col-12 col-xl-4"><span>#คอร์สเรียน</span></div>
                                <div class="col-12 col-xl-8">
                                    <a href="#" class="btn btn-full-pink mr-2 mb-2 interest"><i class="far fa-heart font-icon"></i> สนใจ</a>
                                    <a href="#" class="btn btn-full-red pr-5 btn-read-more mb-2">เริ่มบทเรียน <i class="fas fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="lesson float-full-width">
                <div class="container">
                    <div class="text-header mt-4">
                        <span></span><h1>บทเรียนทั้งหมด</h1>
                    </div>
                    <span class="remark">(แบ่งตามหมวดหมู่)</span>
                    <div class="block-filter float-full-width mt-2">
                        <div class="block-left">
                            <button class="btn btn-filter active"><i class="fas fa-check font-icon mr-2"></i> เลือกทั้งหมด</button>
                            <button class="btn btn-filter active"><i class="fas fa-check font-icon mr-2"></i> โภชนาการ</button>
                            <button class="btn btn-filter active"><i class="fas fa-check font-icon mr-2"></i> พัฒนาทักษะ</button>
                            <button class="btn btn-filter"><i class="fas fa-check font-icon mr-2"></i> ความรู้ทั่วไป</button>
                            <button class="btn btn-filter"><i class="fas fa-check font-icon mr-2"></i> ความรู้เฉพาะทาง</button>
                        </div>
                        <div class="block-right">
                            <span class="float-left mr-2">แสดงบทเรียนที่ปลดล๊อค</span>
                            <div class="switch float-left">
                                <input type="checkbox" id="cbx">
                                <label for="cbx" class="toggle">
                                    <span>
                                    </span>
                                </label>    
                            </div>
                        </div>
                    </div>
                    <div class="block-data-filter float-full-width mt-3">
                        <div class="row">
                            <?php for($i=1;$i<13;$i++) {
                                $class_color = "";
                                $class_lock = "";

                                if($i < 5){
                                    $class_color = "";
                                    $class_lock = "";
                                } else if($i < 7){
                                    $class_color = "yellow";
                                    $class_lock = "lock";
                                }
                                else if($i < 9){
                                    $class_color = "blue";
                                    $class_lock = "lock";
                                }
                                else if($i < 11){
                                    $class_color = "green";
                                    $class_lock = "lock";
                                }
                                else if($i < 13){
                                    $class_color = "red";
                                    $class_lock = "lock";
                                } else {
                                    $class_color = "";
                                    $class_lock = "";
                                }
                            ?>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="content-detail text-center <?php echo $class_color." ".$class_lock; ?>">
                                    <h3>โภชนาการของเด็กโต</h3>
                                    <h5>คอร์สทั่วไป บทที่ 1/8</h5>
                                    <img src="images/home/course/picture-1.png" class="img-fluid mt-2 mb-2">
                                    <p>เรื่อง : หลักการคำนวณโภชนาการของเด็กโต 5 - 10 ขวบ</p>
                                    <div class="content-lock">
                                        <div class="d-flex align-items-center justify-content-center h-100">
                                            <img src="images/elearning/icon-lock.png">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="paginator float-full-width">
                    <div class="container">
                        <div class="d-flex justify-content-between">
                            <div><a href="#" class="arrow"><i class="fas fa-angle-left"></i></a></div>
                            <div>
                                <ul class="pagination">
                                    <li><a href="#" class="active">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                </ul>
                            </div>
                            <div><a href="#" class="arrow active"><i class="fas fa-angle-right"></i></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'layout/ads.php' ?>
        <?php include 'layout/footer.php' ?>
    
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <script src="js/jQuery.mmenu-master/dist/jquery.mmenu.js"></script>
        <script src="js/slick-1.8.1/slick/slick.min.js"></script>
        <script src="js/header_footer.js?t=<?php echo time(); ?>" type="text/javascript"></script>
        <script src="js/elearning.js?t=<?php echo time(); ?>" type="text/javascript"></script>
    </body>
</html>




