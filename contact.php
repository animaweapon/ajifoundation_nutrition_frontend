<!doctype html>
<html lang="en">
    <?php $title_page = 'Contact'; include 'layout/head.php' ?>
    <body>
        <?php include 'layout/header.php' ?>
        <?php 
			$breadcrumbs = [
				['title' => 'หน้าแรก', 'url' => 'home.php'],
				['title' => 'ติดต่อเรา']
			]; 
			include 'layout/breadcrumb.php'
		?>
        <div id="contact" class="float-full-width">
            <div class="data-contact">
                <div class="container">
                    <div class="bg-design bg-1"></div>
                    <div class="row justify-content-center">
                        <div class="wrap-data-contact col-12 col-sm-12 col-md-10 col-lg-8 py-5">
                            <div class="bg-data-contact"></div>
                            <div class="text-header">
                                <span></span><h1>ติดต่อ มูลนิธิอายิโนะโมะโต๊ะ</h1>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-3 col-lg-2">
                                    <b>ที่อยู่ :</b>
                                </div>
                                <div class="col-12 col-sm-9 col-lg-10">
                                    <p>487/1 อาคารศรีอยุธยา ถนนศรีอยุธยา แขวงถนนพญาไท</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-3 col-lg-2">
                                    <b>โทรศัพท์ :</b>
                                </div>
                                <div class="col-12 col-sm-9 col-lg-10">
                                    <p>
                                        <a href="tel:0-2245-1614">0-2245-1614</a>, 
                                        <a href="tel:0-2247-7000">0-2247-7000</a>
                                        ต่อ 1569,1509,1512,1517,1560 หรือ 
                                        <a href="tel:0-2247-9547">0-2247-9547</a>
                                        (สายตรง)
                                    </p>
                                        
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-3 col-lg-2">
                                    <b>โทรสาร :</b>
                                </div>
                                <div class="col-12 col-sm-9 col-lg-10">
                                    <p><a href="fax:02-642-6826">02-642-6826</a></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-3 col-lg-2">
                                    <b>อีเมล์ :</b>
                                </div>
                                <div class="col-12 col-sm-9 col-lg-10">
                                    <p>
                                        <a href="mailto:ajtfoundation@gmail.com">ajtfoundation@gmail.com</a>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <a href="#" class="btn btn-full-red">
                                        More
                                    </a>
                                    <a href="https://maps.google.com/maps?q=AjinomotoFoundation&t=&z=17&ie=UTF8&iwloc=" target="_blank"  class="btn btn-full-black">
                                        Google Map
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-design bg-2"></div>
                </div>
            </div>
            <div class="data-form">
                <div class="container mx-md-100">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-8 py-5">
                            <div class="text-center mb-3">
                                <h1>ติดต่อสอบถามข้อมูลเพิ่มเติม</h1>
                            </div>
                            <form action="#">
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Lastname">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Mobile number">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Subject">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <textarea rows="4" class="form-control" placeholder="Text"></textarea>
                                    </div>
                                </div>
                                <div class="col-12 text-center my-3">
                                    <div class="g-recaptcha d-inline-block" data-sitekey="6LcL5JQUAAAAADXL7LTHUdqzNMhGIzXPpDHpGjPQ"></div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group text-center">
                                        <button class="btn btn-full-red">Submit</button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'layout/ads.php' ?>
        <?php include 'layout/footer.php' ?>
    
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <script src="js/jQuery.mmenu-master/dist/jquery.mmenu.js"></script>
        <script src="js/header_footer.js?t=<?php echo time(); ?>" type="text/javascript"></script>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    </body>
</html>

