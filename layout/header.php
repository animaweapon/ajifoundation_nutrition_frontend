<div id="nav-menu">
    <div class="nav-bg-color"></div>
    <nav class="navbar navbar-expand navbar-light d-none d-xxl-block">
        <div class="container">
            <a class="navbar-brand mt-4" href="#"><img src="images/logo.png" alt="logo"></a>
            <ul class="navbar-nav mr-auto nav-left">
                <li class="nav-item <?php echo ($title_page == 'Home') ? 'active' : '' ?>">
                    <a class="nav-link" href="home.php">หน้าแรก</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Elerning') ? 'active' : '' ?>">
                    <a class="nav-link" href="elearning.php">อีเลิร์นนิ่ง</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Game') ? 'active' : '' ?>">
                    <a class="nav-link" href="#">เกมส์โภชนา</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Tutorial') ? 'active' : '' ?>">
                    <a class="nav-link" href="#">คลังความรู้/สูตรอาหาร</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Gallery') ? 'active' : '' ?>">
                    <a class="nav-link" href="#">รูปภาพกิจกรรม</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Contact') ? 'active' : '' ?>">
                    <a class="nav-link" href="contact.php">ติดต่อเรา</a>
                </li>
            </ul>
            <div class="nav-right">
                <ul class="language navbar-nav my-2 my-lg-0">
                    <li class="nav-item">
                        <a class="nav-link color-pink" href="https://www.ajinomoto.co.th/th"><i class="fas fa-globe-asia font-icon mr-1"></i> AJINOMOTO Thailand</a>
                    </li>
                    <li class="nav-item d-none d-lg-block">
                        <div class="nav-link">|</div>
                    </li>
                    <li class="nav-item d-none d-lg-block">
                        <div class="nav-link">
                            <div class="select-area">
                                <i class="fas fa-angle-down"></i>
                                <select name="language" class="select select-no-frame">
                                    <option value="" selected >Change Language</option>
                                </select>
                            </div>
                        </div>
                    </li>
                </ul>
                <form class="search form-inline my-2 my-lg-0">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                        <input name="search" type="text" class="form-control" placeholder="พิมพ์คำค้นหา">
                    </div>
                    <a class="btn btn-full-black">เข้าสู่ระบบ / สมัครสมาชิก</a>
                </form>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-expand navbar-light d-none d-lg-block d-xxl-none">
        <div class="container">
            <a class="navbar-brand" href="#"><img src="images/logo.png" alt="logo" class="img-fluid"></a>
            <ul class="navbar-nav mr-auto nav-left">
                <li class="nav-item <?php echo ($title_page == 'Home') ? 'active' : '' ?>">
                    <a class="nav-link" href="home.php">หน้าแรก</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Elerning') ? 'active' : '' ?>">
                    <a class="nav-link" href="elearning.php">อีเลิร์นนิ่ง</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Game') ? 'active' : '' ?>">
                    <a class="nav-link" href="#">เกมส์โภชนา</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Tutorial') ? 'active' : '' ?>">
                    <a class="nav-link" href="#">คลังความรู้/สูตรอาหาร</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Gallery') ? 'active' : '' ?>">
                    <a class="nav-link" href="#">รูปภาพกิจกรรม</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Contact') ? 'active' : '' ?>">
                    <a class="nav-link" href="contact.php">ติดต่อเรา</a>
                </li>
            </ul>
            <div class="nav-right">
                <ul class="language navbar-nav my-2 my-lg-0">
                    <li class="nav-item d-none d-lg-block">
                        <div class="nav-link">
                            <div class="select-area">
                                <i class="fas fa-angle-down"></i>
                                <select name="language" class="select select-no-frame">
                                    <option value="" selected >TH</option>
                                </select>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="btn" data-toggle="collapse" data-target="#nav-search" aria-expanded="true" aria-controls="nav-search"><i class="fas fa-search"></i></a>
                    </li>
                    <li class="login">
                        <a href="#" class="btn"><img src="images/icon-user-login.png"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="mobile-nav navbar navbar-expand navbar-light d-block d-lg-none">
        <div class="header">
            <div class="row">
                <div class="col-6">
                    <div class="float-left">        
                        <a class="navbar-brand" href="#"><img src="images/logo.png" alt="logo" class="img-fluid"></a>
                    </div>
                </div>
                <div class="col-6 d-flex justify-content-end align-self-center">
                    <div class="float-right">
                        <div class="d-flex">
                            <a href="#" class="btn" data-toggle="collapse" data-target="#nav-search" aria-expanded="true" aria-controls="nav-search"><img src="images/icon-search-mobile.png"></a>
                            <a href="#" class="btn"><img src="images/icon-user-login-mobile.png"></i></a>
                            <a id="mobile-menu-icon" href="#menu-mobile" class="btn"><img src="images/icon-menu.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav id="menu-mobile">
            <ul>
                <li class="nav-item <?php echo ($title_page == 'Home') ? 'active' : '' ?>">
                    <a class="nav-link" href="home.php">หน้าแรก</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Elerning') ? 'active' : '' ?>">
                    <a class="nav-link" href="elearning.php">อีเลิร์นนิ่ง</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Game') ? 'active' : '' ?>">
                    <a class="nav-link" href="#">เกมส์โภชนา</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Tutorial') ? 'active' : '' ?>">
                    <a class="nav-link" href="#">คลังความรู้/สูตรอาหาร</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Gallery') ? 'active' : '' ?>">
                    <a class="nav-link" href="#">รูปภาพกิจกรรม</a>
                </li>
                <li class="nav-item <?php echo ($title_page == 'Contact') ? 'active' : '' ?>">
                    <a class="nav-link" href="contact.php">ติดต่อเรา</a>
                </li>
                <li class="nav-language nav-item">
                    <span class="d-block float-left padding-style">
                        Change Language 
                    </span>
                    <span class="d-block float-right">
                        <a href="#" class="btn">EN</a>
                        <a href="#" class="btn active">TH</a>
                    </span>
                </li>
            </ul>
        </nav>
    </div>
    <div class="block-search float-full-width">
        <div class="container">
            <div id="nav-search" class="collapse in" aria-expanded="true">
                <form id="searchFormMobile" action="https://www.ajinomoto.co.th/th/search" method="get">
                <div class="search-field">
                    <div class="input-group">
                        <input name="q" type="text" class="search-input form-control" placeholder="พิมพ์คำค้นหา">
                        <span class="input-group-btn">
                            <button class="btn btn-search" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </span>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>