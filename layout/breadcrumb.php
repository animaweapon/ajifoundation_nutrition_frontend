<div class="container mx-md-100">
    <div class="row">
        <div class="col-auto">
            <div class="wrap-breadcrumb">
                <ul class="breadcrumb">
                    <?php foreach($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        <?php if(!empty($breadcrumb['url'])) { ?>
                            <a href="<?php echo $breadcrumb['url'] ?>"><?php echo $breadcrumb['title'] ?></a>
                        <?php } else { ?>
                            <span><?php echo $breadcrumb['title'] ?></span>
                        <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>