<section class="ads float-full-width">
    <div class="bg-design bg-1"></div>
    <div class="container">
        <h1 class="text-center">รสชาติพื้นฐานที่ 5</h1>
        <p class="text-center mb-4">การปรุงรสชาติอาหารคือการหยิบยื่นโภชนาการให้กับทุกคน</p>
        <div class="d-md-flex align-items-center justify-content-center text-center">
            <div>
                <img src="images/taste/picture-1.png" class="image image-1">
            </div>
            <a href="#" class="btn btn-full-white pl-5 pr-5"><span class="float-left">ทำความรู้จัก</span>  <img src="images/taste/icon-umami.png" class="img-fluid"></a>
            <img src="images/taste/picture-2.png" class="image image-2 d-none d-sm-block">
        </div>
    </div>
    <div class="bg-design bg-2"></div>
</section>