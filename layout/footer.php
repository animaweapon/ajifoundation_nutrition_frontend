<footer>
    <div class="nav-bg-color"></div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="about col-12 col-sm-6 col-lg-5">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-lg-5">
                            <a href="#" class="logo"><img src="images/logo.png" alt="logo" class="img-fluid"></a>
                        </div>
                        <div class="col-12 col-sm-12 col-lg-5">
                            <p class="mt-3">487/1 อาคารศรีอยุธยา ถนนศรีอยุธยา
                                    แขวงถนนพญาไท เขตราชเทวี
                                    กรุงเทพฯ 10400</p>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 col-lg-10">
                            <div class="google-map">
                                <iframe class="w-100 h-100" 
                                    src="https://maps.google.com/maps?q=AjinomotoFoundation&t=&z=17&ie=UTF8&iwloc=&output=embed" 
                                    frameborder="0"
                                    style="border:0" 
                                    allowfullscreen>
                                </iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-7">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-lg-6 col-xl-4">
                                    <ul class="list-link list-unstyled">
                                        <li>มูลนิธิอายิโนะโมะโต๊ะ</li>
                                        <li><a href="#">คำถามที่พบบ่อย</a></li>
                                        <li><a href="#">ติดต่อเรา</a></li>
                                        <li><a href="#">แผนผังเวปไซต์</a></li>
                                    </ul>
                                </div>
                                <div class="col-12 col-lg-6 col-xl-4">
                                    <ul class="list-link list-unstyled">
                                        <li>ลิงค์ที่เกี่ยวข้อง</li>
                                        <li><a href="#">เว็ปไซต์อายิโนะโมะโต๊ะ (ประเทศไทย)</a></li>
                                        <li><a href="#">AJINOMOTO Global Home</a></li>
                                        <li><a href="#">AJINOMOTO Global Story</a></li>
                                    </ul>
                                </div>
                                <div class="list-social col-12 col-lg-12 col-xl-4">
                                    <ul class="list-inline">
                                        <li>ติดตามเราได้ที่</li>
                                        <li class="list-inline-item"><a href="#"><img src="images/footer-icon-facebook.png"></a></li>
                                        <li class="list-inline-item"><a href="#"><img src="images/footer-icon-ig.png"></a></li>
                                        <li class="list-inline-item"><a href="#"><img src="images/footer-icon-youtube.png"></a></li>
                                        <li class="list-inline-item"><a href="#"><img src="images/footer-icon-twiiter.png"></a></li>
                                    </ul>
                                    <a href="#" class="btn btn-up btn-frame-gray">Back to top <i class="fas fa-angle-up"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="terms">
                        <a href="#">เงื่อนไขและบริการ</a>
                        <span>|</span>
                        <a href="#">นโยบายความเป็นส่วนตัว</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="copy-right">
                        © สงวนลิขสิทธิ์ 2562 มูลนิธิอายิโนะโมะโต๊ะ
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>