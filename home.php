<!doctype html>
<html lang="en">
    <?php $title_page = 'Home'; include 'layout/head.php' ?>
    <body>
        <?php include 'layout/header.php' ?>

        <div id="home">
            <section class="banner float-full-width">
                <div class="container">
                    <div class="block-banner">
                        <img src="images/home/banner/banner-1.jpg" class="img-fluid">
                        <img src="images/home/banner/banner-2.jpg" class="img-fluid">
                    </div>
                </div>
            </section>
            <section class="activity float-full-width">
                <div class="container">
                    <div class="text-header">
                        <span></span><h1>กิจกรรมล่าสุด</h1>
                    </div>
                    <div class="block-activity">
                        <div class="activity-content">
                            <div class="row getArea">
                                <div class="col-4"><img src="images/home/activity/activity-1.png" class="img-fluid"></div>
                                <div class="col-8">
                                    <h3>โภชนาการของเด็กโต</h3>
                                    <h5>คอร์สทั่วไป บทที่ 2/8</h5>
                                    <p>เรื่อง : หลักการคำนวณโภชนาการของเด็กโต 5 - 10 ขวบ</p>
                                </div>
                            </div>
                            <div class="row hashtag">
                                <div class="col-12 col-xl-4"><span>#คอร์สเรียน</span></div>
                                <div class="col-12 col-xl-8">
                                    <a href="#" class="btn btn-full-pink mr-2 mb-2 interest"><i class="far fa-heart font-icon"></i> สนใจ</a>
                                    <a href="#" class="btn btn-full-red pr-5 btn-read-more mb-2">เริ่มบทเรียน <i class="fas fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="activity-content">
                            <div class="row getArea">
                                <div class="col-12">
                                    <h3>โครงการ : ทุนส่งน้องเรียนจบ</h3>
                                    <p>การศึกษา ถือเป็นรากฐานที่สำคัญยิ่งของการพัฒนาประเทศ
                                            และเป็นแรงขับเคลื่อนที่สำคัญในการปูพื้นฐานให้มั่นคงพร้อมที่
                                            จะนำพาประเทศชาติของเรา ..</p>
                                </div>
                            </div>
                            <div class="row hashtag">
                                <div class="col-12 col-xl-4"><span>#ข่าวกิจกรรม</span></div>
                                <div class="col-12 col-xl-8">
                                    <a href="#" class="btn btn-full-pink mr-2 mb-2 interest"><i class="far fa-heart font-icon"></i> สนใจ</a>
                                    <a href="#" class="btn btn-full-red pr-5 btn-read-more mb-2">อ่านต่อ <i class="fas fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="activity-content">
                            <div class="row getArea">
                                <div class="col-4"><img src="images/home/activity/activity-2.png" class="img-fluid"></div>
                                <div class="col-8">
                                    <h3>บะหมี่เกี๊ยวหมูแดงข้าวหมูแดง</h3>
                                    <p>ผศ.มาริน สาลี</p>
                                </div>
                            </div>
                            <div class="row hashtag">
                                <div class="col-12 col-xl-4"><span>#สูตรและเมนูอาหาร</span></div>
                                <div class="col-12 col-xl-8">
                                    <a href="#" class="btn btn-full-pink mr-2 mb-2 interest"><i class="far fa-heart font-icon"></i> สนใจ</a>
                                    <a href="#" class="btn btn-full-red pr-5 btn-read-more mb-2">ดูวิธีการทำ <i class="fas fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="activity-content">
                            <div class="row getArea">
                                <div class="col-4"><img src="images/home/activity/activity-2.png" class="img-fluid"></div>
                                <div class="col-8">
                                    <h3>บะหมี่เกี๊ยวหมูแดงข้าวหมูแดง</h3>
                                    <p>ผศ.มาริน สาลี</p>
                                </div>
                            </div>
                            <div class="row hashtag">
                                <div class="col-12 col-xl-4"><span>#สูตรและเมนูอาหาร</span></div>
                                <div class="col-12 col-xl-8">
                                    <a href="#" class="btn btn-full-pink mr-2 mb-2 interest"><i class="far fa-heart font-icon"></i> สนใจ</a>
                                    <a href="#" class="btn btn-full-red pr-5 btn-read-more mb-2">ดูวิธีการทำ <i class="fas fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="course float-full-width">
                <div class="row justify-content-center">
                    <div class="col-2 d-none d-sm-flex align-items-end justify-content-center">
                        <img src="images/home/course/object-1.png" class="img-fluid">
                    </div>
                    <div class="col-10 col-sm-8">
                        <div class="wrap-filter row no-gutters">
                            <div class="col-12">
                                <div class="text-center">
                                    <div class="text-header">
                                        <span></span><h1>คอร์สที่คุณสนใจ</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center mt-2">
                                <div class="select-area">
                                    <i class="fas fa-angle-down"></i>
                                    <select class="select select-white">
                                        <option value="" selected >โภชนาการของเด็กโต</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-full-red">เลือก</button>
                                <a href="#" class="btn btn-full-gray">ลบ</a>
                            </div>
                        </div>
                        <div class="block-course mt-4">
                            <div class="content-detail text-center">
                                <h3>โภชนาการของเด็กโต 1</h3>
                                <h5>คอร์สทั่วไป บทที่ 1/8</h5>
                                <img src="images/home/course/picture-1.png" class="img-fluid mt-2 mb-2">
                                <p>เรื่อง : หลักการคำนวณโภชนาการของเด็กโต 5 - 10 ขวบ</p>
                            </div>
                            <div class="content-detail text-center lock">
                                <h3>โภชนาการของเด็กโต 2</h3>
                                <h5>คอร์สทั่วไป บทที่ 1/8</h5>
                                <img src="images/home/course/picture-1.png" class="img-fluid mt-2 mb-2">
                                <p>เรื่อง : หลักการคำนวณโภชนาการของเด็กโต 5 - 10 ขวบ</p>
                                <div class="content-lock">
                                    <div class="d-flex align-items-center justify-content-center h-100">
                                        <img src="images/home/course/icon-lock.png">
                                    </div>
                                </div>
                            </div>
                            <div class="content-detail text-center lock">
                                <h3>โภชนาการของเด็กโต 3</h3>
                                <h5>คอร์สทั่วไป บทที่ 1/8</h5>
                                <img src="images/home/course/picture-1.png" class="img-fluid mt-2 mb-2">
                                <p>เรื่อง : หลักการคำนวณโภชนาการของเด็กโต 5 - 10 ขวบ</p>
                                <div class="content-lock">
                                    <div class="d-flex align-items-center justify-content-center h-100">
                                        <img src="images/home/course/icon-lock.png">
                                    </div>
                                </div>
                            </div>
                            <div class="content-detail text-center lock">
                                <h3>โภชนาการของเด็กโต 4</h3>
                                <h5>คอร์สทั่วไป บทที่ 1/8</h5>
                                <img src="images/home/course/picture-1.png" class="img-fluid mt-2 mb-2">
                                <p>เรื่อง : หลักการคำนวณโภชนาการของเด็กโต 5 - 10 ขวบ</p>
                                <div class="content-lock">
                                    <div class="d-flex align-items-center justify-content-center h-100">
                                        <img src="images/home/course/icon-lock.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-2 d-none d-sm-flex align-items-end justify-content-center">
                        <img src="images/home/course/object-2.png" class="img-fluid">
                    </div>
                </div>
            </section>
            <section class="game float-full-width">
                <div class="container">
                    <div class="text-header">
                        <span></span><h1>เกมส์โภชนาการ</h1>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 mb-4">
                            <a href="#" class="game-link game-1 d-flex align-items-center justify-content-center">
                                <img src="images/home/game/picture-1.png" class="img-fluid">
                                <h3>เกมส์จับคู่ (Matching)</h3>
                            </a>
                        </div>
                        <div class="col-12 col-sm-6 mb-4">
                            <a href="#" class="game-link game-2 d-flex align-items-center justify-content-center">
                                <img src="images/home/game/picture-2.png" class="img-fluid">
                                <h3>เกมส์ทายแคลอรี่</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <section class="recipe float-full-width">
                <div class="container">
                    <div class="text-header">
                        <span></span><h1>สูตรและเมนูอาหาร</h1>
                    </div>
                    <div class="float-right">
                        <a href="#" class="link-see-more">ดูเมนูทั้งหมด</a>
                    </div>
                    <div class="block-recipe">
                        <div class="content-recipe">
                            <div class="image-content">
                                <img src="images/home/recipe/picture-1.jpg" class="img-fluid">
                            </div>
                            <h3 class="mt-2 mb-2">ซูชิคุณหนู</h3>
                            <div class="share-content d-flex align-items-center justify-content-center">
                                <div class="view mr-3">
                                    <button class="btn">
                                        <img src="images/home/recipe/icon-view.png">
                                        <span>0</span>
                                    </button>
                                </div>
                                <div class="love mr-3">
                                    <button class="btn active">
                                        <img src="images/home/recipe/icon-love.png" class="no-active">
                                        <img src="images/home/recipe/icon-love-active.png" class="active">
                                        <span>0</span>
                                    </button>
                                </div>
                                <div class="share">
                                    <button class="btn active">
                                        <img src="images/home/recipe/icon-share.png" class="no-active">
                                        <img src="images/home/recipe/icon-share-active.png" class="active">
                                        <span>0</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="content-recipe">
                            <div class="image-content">
                                <img src="images/home/recipe/picture-2.jpg" class="img-fluid">
                            </div>
                            <h3 class="mt-2 mb-2">ขนมจีบหมู</h3>
                            <div class="share-content d-flex align-items-center justify-content-center">
                               <div class="view mr-3">
                                    <button class="btn">
                                        <img src="images/home/recipe/icon-view.png">
                                        <span>0</span>
                                    </button>
                                </div>
                                <div class="love mr-3">
                                    <button class="btn active">
                                        <img src="images/home/recipe/icon-love.png" class="no-active">
                                        <img src="images/home/recipe/icon-love-active.png" class="active">
                                        <span>0</span>
                                    </button>
                                </div>
                                <div class="share">
                                    <button class="btn">
                                        <img src="images/home/recipe/icon-share.png" class="no-active">
                                        <img src="images/home/recipe/icon-share-active.png" class="active">
                                        <span>0</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="content-recipe">
                            <div class="image-content">
                                <img src="images/home/recipe/picture-3.jpg" class="img-fluid">
                            </div>
                            <h3 class="mt-2 mb-2">ผัดผักรวมมิตร</h3>
                            <div class="share-content d-flex align-items-center justify-content-center">
                                <div class="view mr-3">
                                    <button class="btn">
                                        <img src="images/home/recipe/icon-view.png">
                                        <span>0</span>
                                    </button>
                                </div>
                                <div class="love mr-3">
                                    <button class="btn active">
                                        <img src="images/home/recipe/icon-love.png" class="no-active">
                                        <img src="images/home/recipe/icon-love-active.png" class="active">
                                        <span>0</span>
                                    </button>
                                </div>
                                <div class="share">
                                    <button class="btn">
                                        <img src="images/home/recipe/icon-share.png" class="no-active">
                                        <img src="images/home/recipe/icon-share-active.png" class="active">
                                        <span>0</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="content-recipe">
                            <div class="image-content">
                                <img src="images/home/recipe/picture-4.jpg" class="img-fluid">
                            </div>
                            <h3 class="mt-2 mb-2">ข้าวมันไก่ไหหลำ + น้ำจิ้มรสเด็ด</h3>
                            <div class="share-content d-flex align-items-center justify-content-center">
                                <div class="view mr-3">
                                    <button class="btn">
                                        <img src="images/home/recipe/icon-view.png">
                                        <span>0</span>
                                    </button>
                                </div>
                                <div class="love mr-3">
                                    <button class="btn">
                                        <img src="images/home/recipe/icon-love.png" class="no-active">
                                        <img src="images/home/recipe/icon-love-active.png" class="active">
                                        <span>0</span>
                                    </button>
                                </div>
                                <div class="share">
                                    <button class="btn active">
                                        <img src="images/home/recipe/icon-share.png" class="no-active">
                                        <img src="images/home/recipe/icon-share-active.png" class="active">
                                        <span>0</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="content-recipe">
                            <div class="image-content">
                                <img src="images/home/recipe/picture-2.jpg" class="img-fluid">
                            </div>
                            <h3 class="mt-2 mb-2">ซูชิคุณหนู</h3>
                            <div class="share-content d-flex align-items-center justify-content-center">
                                <div class="view mr-3">
                                    <button class="btn">
                                        <img src="images/home/recipe/icon-view.png">
                                        <span>0</span>
                                    </button>
                                </div>
                                <div class="love mr-3">
                                    <button class="btn active">
                                        <img src="images/home/recipe/icon-love.png" class="no-active">
                                        <img src="images/home/recipe/icon-love-active.png" class="active">
                                        <span>0</span>
                                    </button>
                                </div>
                                <div class="share">
                                    <button class="btn">
                                        <img src="images/home/recipe/icon-share.png" class="no-active">
                                        <img src="images/home/recipe/icon-share-active.png" class="active">
                                        <span>0</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="gallery float-full-width">
                <div class="text-center">
                    <div class="text-header">
                        <span></span><h1>รูปภาพกิจกรรมล่าสุด</h1>
                    </div>
                </div>
                <div class="block-gallery">
                    <div class="content-gallery">
                        <img src="images/home/gallery/picture-1.png">
                        <div class="detail-content">
                            <h3>โครงการ : ทุนส่งน้องเรียนจบ</h3>
                            <p>การศึกษา ถือเป็นรากฐานที่สำคัญยิ่งของการพัฒนาประเทศ และเป็นแรงขับเคลื่อนที่สำคัญในการปูพื้นฐานให้มั่นคงพร้อมที่จะนำพาประเทศชาติของเรา ..</p>
                        </div>
                        <div class="fade-bg"></div>
                    </div>
                    <div class="content-gallery">
                        <img src="images/home/gallery/picture-2.png">
                        <div class="detail-content">
                            <h3>โครงการ : ทุนส่งน้องเรียนจบ</h3>
                            <p>การศึกษา ถือเป็นรากฐานที่สำคัญยิ่งของการพัฒนาประเทศ และเป็นแรงขับเคลื่อนที่สำคัญในการปูพื้นฐานให้มั่นคงพร้อมที่จะนำพาประเทศชาติของเรา ..</p>
                        </div>
                        <div class="fade-bg"></div>
                    </div>
                    <div class="content-gallery">
                        <img src="images/home/gallery/picture-3.png">
                        <div class="detail-content">
                            <h3>โครงการ : ทุนส่งน้องเรียนจบ</h3>
                            <p>การศึกษา ถือเป็นรากฐานที่สำคัญยิ่งของการพัฒนาประเทศ และเป็นแรงขับเคลื่อนที่สำคัญในการปูพื้นฐานให้มั่นคงพร้อมที่จะนำพาประเทศชาติของเรา ..</p>
                        </div>
                        <div class="fade-bg"></div>
                    </div>
                </div>
                <div class="mt-4 text-center">
                    <a href="#" class="link-see-more">ดูภาพกิจรรมทั้งหมด</a>
                </div>
            </section>
        </div>
       
        <?php include 'layout/ads.php' ?>
        <?php include 'layout/footer.php' ?>
    
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <script src="js/jQuery.mmenu-master/dist/jquery.mmenu.js"></script>
        <script src="js/slick-1.8.1/slick/slick.min.js"></script>
        <script src="js/header_footer.js?t=<?php echo time(); ?>" type="text/javascript"></script>
        <script src="js/home.js?t=<?php echo time(); ?>" type="text/javascript"></script>
    </body>
</html>

