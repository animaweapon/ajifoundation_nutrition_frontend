$(document).ready(function(){
    $('.btn.interest').click(function(e) {
        e.preventDefault();
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    });

    $('.btn.btn-filter').click(function(e) {
        e.preventDefault();
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    });

    $('#cbx').change(function(e) {
        if($(this).is(':checked')){
            $.each($('.content-detail'), function() {
                if($(this).hasClass('lock')){
                    $(this).parent().addClass('d-none').removeClass('d-block');
                }
            });
        } else {
            $.each($('.content-detail'), function() {
                if($(this).hasClass('lock')){
                    $(this).parent().removeClass('d-none').addClass('d-block');
                }
            });
        }
    });

    $('.block-activity').slick({
        dots: true,
        arrows: false,
        slidesToShow: getSlidesToShow('block-activity'),
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
    });

    getAreaRowActivity();
    getAreaContentDetail();

    function getAreaRowActivity(){
        var maxHeight = 0;
        $('.getArea').each(function(){
            var thisH = $(this).height();
            if (thisH > maxHeight) { maxHeight = thisH; }
        });
        $('.getArea').height(maxHeight);
    }

    function getAreaContentDetail(){
        var maxHeight = 0;
        $('.content-detail').each(function(){
            var thisH = $(this).height();
            if (thisH > maxHeight) { maxHeight = thisH; }
        });
        $('.content-detail').height(maxHeight);
    }

    function getSlidesToShow(slideName){
        var windowWidth = $(window).width();
        switch(slideName) {
            case 'block-activity':
                if(windowWidth <= 450){
                    return 1;
                } else if(windowWidth <= 800){
                    return 2;
                } else {
                    return 3;
                }
                break;
            default:
                break;
        }
    }

    $( window ).resize(function() {
        $('.block-activity').slick("unslick");
        $('.block-activity').slick({
            dots: true,
            arrows: false,
            slidesToShow: getSlidesToShow('block-activity'),
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
        });
        
        $('.getArea').height('auto');
        $('.content-detail').height('auto');
        setTimeout(function(){ 
            getAreaRowActivity();
            getAreaContentDetail();
        }, 200);
    });

});