$(document).ready(function(){
    $("#menu-mobile").mmenu({
        extensions  : ["fullscreen", "position-front"],
        wrappers: ["bootstrap3"],
        navbar: {
            title: ""
        }
    });

    $(".btn-up").click(function () {
        $("html, body").animate({scrollTop: 0}, 1000);
    });

});