$(document).ready(function(){
    $('.btn.interest').click(function(e) {
        e.preventDefault();
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    });
    
    $('.block-banner').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        arrows: false,
    });

    $('.block-activity').slick({
        dots: true,
        arrows: false,
        slidesToShow: getSlidesToShow('block-activity'),
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
    });

    $('.block-course').slick({
        slidesToShow: getSlidesToShow('block-course'),
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        focusOnSelect: true,
        centerMode: true,
        centerPadding: 0,
    });

    $('.block-recipe').slick({
        dots: true,
        arrows: false,
        slidesToShow: getSlidesToShow('block-recipe'),
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
    });

    $('.block-gallery').slick({
        dots: false,
        arrows: false,
        centerMode: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        focusOnSelect: true,
    });

    getAreaRowActivity();

    function getAreaRowActivity(){
        var maxHeight = 0;
        $('.getArea').each(function(){
            var thisH = $(this).height();
            if (thisH > maxHeight) { maxHeight = thisH; }
        });
        $('.getArea').height(maxHeight);
    }

    function getSlidesToShow(slideName){
        var windowWidth = $(window).width();
        switch(slideName) {
            case 'block-activity':
                if(windowWidth <= 450){
                    return 1;
                } else if(windowWidth <= 800){
                    return 2;
                } else {
                    return 3;
                }
                break;
            case 'block-course':
                if(windowWidth <= 450){
                    return 1;
                } else if(windowWidth <= 800){
                    return 2;
                } else {
                    return 3;
                }
                break;
            case 'block-recipe':
                if(windowWidth <= 450){
                    return 1;
                } else if(windowWidth <= 600){
                    return 2;
                } else if(windowWidth <= 800){
                    return 3;
                } else {
                    return 4;
                }
                break;
            default:
                break;
        }
    }

    $( window ).resize(function() {
        $('.getArea').height('auto');
        setTimeout(function(){ getAreaRowActivity(); }, 200);

        $('.block-activity').slick("unslick");
        $('.block-activity').slick({
            dots: true,
            arrows: false,
            slidesToShow: getSlidesToShow('block-activity'),
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
        });

        $('.block-course').slick("unslick");
        $('.block-course').slick({
            slidesToShow: getSlidesToShow('block-course'),
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            focusOnSelect: true,
            centerMode: true,
            centerPadding: 0,
        });
    
        $('.block-recipe').slick("unslick");
        $('.block-recipe').slick({
            dots: true,
            arrows: false,
            slidesToShow: getSlidesToShow('block-recipe'),
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
        });

    });

});