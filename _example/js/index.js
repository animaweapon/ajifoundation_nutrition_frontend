/*
* Example Frontend Template 
* Version: 1.0
* Updated: 22/02/2019
*/

//------------------------- INIT -------------------------//
var stageWidth = 0;
var stageHeight = 0;
var $body;
var $header;
var $home;
var $client;
var $contact;
var $footer;

$(document).ready(function(){
	initVariable();
	initEvent();
});

//------------------------- INIT VARIABLE -------------------------//
function initVariable(){
    $body = $("body");
	$header = $("header");
    $home = $("#home");
    $client = $("#client");
    $contact = $("#contact");
    $footer = $("footer");
}

//------------------------- INIT EVENT -------------------------//
function initEvent(){
	/* resize */
	onResize();
	$(window).resize(onResize);
	/* nav */
    $body.scrollspy({target: ".navbar", offset: 100});
    $(".navbar a.nav-link").on('click', scrollPage);
}

//------------------------- PAGE -------------------------//
function scrollPage(event) {
    if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash & animate scroll page
        var hash = this.hash;
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800, function(){
            window.location.hash = hash;
        });
    }
}

//------------------------- RESIZE -------------------------//
function onResize(){
	resizeWindow();
}

function resizeWindow(){
	stageWidth = $(window).width();
	stageHeight = $(window).height();
	console.log("w: "+stageWidth+" h: "+stageHeight);
}