<?php
    $banners = Array(
        Array(
            'title' => 'Banner 1',
            'color1' => '#777',
            'color2' => '#bbb',
        ),
        Array(
            'title' => 'Banner 2',
            'color1' => '#666',
            'color2' => '#aaa',
        ),
        Array(
            'title' => 'Banner 3',
            'color1' => '#555',
            'color2' => '#999',
        ),
        Array(
            'title' => 'Banner 4',
            'color1' => '#444',
            'color2' => '#888',
        ),
    );

    $clients = Array(
        Array(
            'image' => 'images/clients/logo-1.png',
            'title' => 'DEP',
        ),
        Array(
            'image' => 'images/clients/logo-2.png',
            'title' => 'BANGKOK BANK',
        ),
        Array(
            'image' => 'images/clients/logo-3.png',
            'title' => 'CHIA TAI',
        ),
        Array(
            'image' => 'images/clients/logo-4.png',
            'title' => 'MUSEUM SIAM',
        ),
        Array(
            'image' => 'images/clients/logo-5.png',
            'title' => 'THAILAND POST',
        ),
        Array(
            'image' => 'images/clients/logo-6.png',
            'title' => 'KANTANA MOTION PICTURES',
        ),
        Array(
            'image' => 'images/clients/logo-7.png',
            'title' => 'SHELL',
        ),
        Array(
            'image' => 'images/clients/logo-8.png',
            'title' => 'TRUE',
        ),
        Array(
            'image' => 'images/clients/logo-9.png',
            'title' => 'CPN',
        ),
        Array(
            'image' => 'images/clients/logo-10.png',
            'title' => 'BANPU',
        ),
        Array(
            'image' => 'images/clients/logo-11.png',
            'title' => 'MoMA',
        ),
        Array(
            'image' => 'images/clients/logo-12.png',
            'title' => 'EAST WATER',
        ),
    );
?>