<?php include 'data.php' ?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->
	<head>
		<!-- Basic Page Needs
		================================================== -->
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
		<meta name="keywords" content="example / html5 / scss / bootstrap4 / jquery3.3.*"/>
		<meta name="description" content="Show client list" />
		
		<title>Example</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >		
		<meta property="og:title" content="Example" >
        <meta property="og:type" content="article">
		<meta property="og:description" content="Frontend Template" >
        <meta property="og:image" content="https://a.deviantart.net/avatars/a/n/animaweapon.jpg?4" >
        <meta property="og:determiner" content="auto" >		
		
		<!-- Mobile Specific Metas
		================================================== -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

		<!-- Fonts
		================================================== -->
		<link rel="stylesheet" href="fonts/notosanskr.css"/>
        <link rel="stylesheet" href="fonts/logo.css"/>
		
		<!-- CSS
		================================================== -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
		<link rel="stylesheet" href="css/index.css?t=<?php echo time(); ?>"/>
		
		<!-- Favicons
		================================================== -->
		<link rel="shortcut icon" href="images/favicon.ico"/>

	</head>
	<body>
        <header class="container-fluid fixed-top">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col brand pr-0">
                            <div class="row">
                                <div class="logo col-auto pr-0">
                                    <span>LOGO</span>
                                </div>
                                <div class="vertical-line col-auto"></div>
                                <div class="tagline col-auto px-0">
                                    <span>
                                        Show client list<br/>
                                        as sample template
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto pl-0">
                            <nav id="navbar" class="navbar">
                                <a id="nav-home" class="nav-link active" href="#home">Home</a>
                                <a id="nav-client" class="nav-link" href="#client">Client</a>
                                <a id="nav-contact" class="nav-link" href="#contact">Contact</a>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
		<div id="home" class="container-fluid">
            <div class="row">
                <div class="col p-0">
                    <div class="bd-example">
                      <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <?php foreach ($banners as $i => $banner) { ?>
                            <li data-target="#carouselExampleCaptions" data-slide-to="<?php echo $i; ?>" class="<?php echo $i==0 ? 'active' : ''; ?>"></li>
                            <?php } ?>
                        </ol>
                        <div class="carousel-inner">
                            <?php foreach ($banners as $i => $banner) { ?>
                            <div class="carousel-item <?php echo $i==0 ? 'active' : ''; ?>" data-interval="">
                                <svg class="bd-placeholder-img bd-placeholder-img-lg d-block w-100" 
                                width="800" height="400" 
                                xmlns="http://www.w3.org/2000/svg" 
                                preserveAspectRatio="xMidYMid slice" 
                                focusable="false" 
                                role="img" 
                                aria-label="Placeholder: Slide">
                                    <title>Placeholder</title>
                                    <rect fill="<?php echo $banner['color1']; ?>" width="100%" height="100%"></rect>
                                    <text fill="<?php echo $banner['color2']; ?>" dy=".3em" x="50%" y="50%" class="txt-thin-36"><?php echo $banner['title']; ?></text>
                                </svg>
                            </div>
                            <?php } ?>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                      </div>
                    </div>
                </div>
            </div>
		</div>
        <div id="client" class="container">
            <div class="row">
                <div class="col">
                    <h1 class="text-center">Client</h1>
                    <div class="row">
                        <?php foreach ($clients as $client) { ?>
                        <div class="thumbnail col-4 col-sm-3 col-lg-2">
                            <div class="image">
                                <img src="<?php echo $client['image']; ?>" class="img-fluid" alt="<?php echo $client['title']; ?>">
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="contact" class="container">
            <hr/>
            <div class="row">
                <div class="col">
                    <h1 class="text-center">Contact</h1>
                    <ul>
                        <li><b>•</b> Morakot KK.</li>
                        <li><b>•</b> Developer / Designer</li>
                        <li><b>•</b> Work @home</li>
                    </ul>
                    <p class="pl-14px mb-5">
                        <a href="mailto:pic.morakot@gmail.com">pic.morakot@gmail.com</a><br/>
                        <a href="tel:+66636610351">T. +666 366  10351</a>
                    </p>
                    <p class="mb-5">
                        Visit our showroom, please call or e-mail<br/>
                        in advance for appointment
                    </p>
                    <div class="google-map">
                        <iframe class="w-100 h-100" 
                        src="https://maps.google.com/maps?q=MetroSkyPrachachuen&t=&z=17&ie=UTF8&iwloc=&output=embed" 
                        frameborder="0"
                        style="border:0" 
                        allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <footer class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <span class="copyright">© 2019 Example Co., Ltd. All rights reserved.</span>
                        </div>
                        <div class="col-auto">
                            <a class="btn btn-social btn-instagram" href="#"></a>
                            <a class="btn btn-social btn-facebook" href="#"></a>
                            <a class="btn btn-social btn-mail" href="#"></a>
                            <a class="btn btn-social btn-pinterest" href="#"></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
		<!-- JS
		================================================== -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <script src="js/index.js?t=<?php echo time(); ?>" type="text/javascript"></script>
	</body>
</html>

