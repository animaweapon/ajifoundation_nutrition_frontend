<!doctype html>
<html lang="en">
	<?php $title_page = 'Profile'; include 'layout/head.php' ?>
	<body class="bg-light-gray">        
		<?php include 'layout/header.php' ?>
		<?php 
			$breadcrumbs = [
				['title' => 'หน้าแรก', 'url' => 'home.php'],
				['title' => 'ระบบสมาชิก', 'url' => '#'],
				['title' => 'สถิติอีเลิร์นนิ่ง']
			]; 
			include 'layout/breadcrumb.php'
		?>
		<div id="profile" class="container mx-md-100">
			<div class="row">
				<div class="col-md-3">
					<div class="ctn-content mb-3">
						<div class="ct-profile">
							<div class="row">
								<div class="col-12">
									<div class="bx-name">
										<div class="lb-profile">สวัสดี!</div>
										<div class="lb-name">คุณ สมชาย  พัฒนายั่งยืน</div>
									</div>
								</div>
								<div class="col-12">
									<div class="line"></div>
								</div>
								<div class="col-12">
									<div class="lb-lg float-left">เข้าสู่ระบบล่าสุด</div>
									<div class="lb-lg float-right">02/02/2562</div>
								</div>
								<div class="col-12">
									<div class="bx-btn">
										<button type="button">แก้ไขข้อมูลส่วนตัว</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="ctn-content mb-5">
						<div class="ct-ctrl ct-stat">
							<h2>สถิติอีเลิร์นนิ่งทั้งหมด</h2>
							<div class="card-deck ls-stat">
								<div class="card item text-center">
									<div class="card-body item-inn p-3">
										<h3 class="lb-stat mb-0">คะแนนสะสม</h3>
										<div class="ic-profile ic-stat-1 mt-2 mx-auto"></div>
									</div>
									<div class="card-footer">
										<div class="bx-point">
											<div class="td align-middle">
												<div class="lb-point">2,800</div>
												<div class="lb-unit">คะแนน</div>
											</div>
										</div>
									</div>
								</div>
								<div class="card item text-center">
									<div class="card-body item-inn p-3">
										<h3 class="lb-stat mb-0">คอร์สที่เรียนทั้งหมด</h3>
										<div class="ic-profile ic-stat-2 mt-2 mx-auto"></div>
									</div>
									<div class="card-footer">
										<div class="bx-point">
											<div class="td col align-middle">
												<div class="lb-point">2</div>
												<div class="lb-unit">คอร์ส</div>
											</div>
											<div class="td col align-middle">
												<div class="lb-point">8</div>
												<div class="lb-unit">บท</div>
											</div>
										</div>
									</div>
								</div>
								<div class="card item text-center">
									<div class="card-body item-inn p-3">
										<h3 class="lb-stat mb-0">สติ๊กเกอร์ที่ได้รับล่าสุด</h3>
										<div class="mx-auto ic-profile ic-sticker ic-sticker-5 mt-2"></div>
									</div>
									<div class="card-footer">
										<div class="bx-point">
											<div class="td align-middle">
												<div class="lb-point2">The two star</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="ct-ctrl ct-sticker">
							<h2>สติ๊กเกอร์ที่สะสมได้</h2>
							<div class="ls-sticker">
								<div class="ls-sticker-inn row">
									<div class="item col-6 col-sm-4 p-3 text-center">
										<div class="mx-auto ic-profile ic-sticker ic-sticker-1"></div>
										<div class="lb-sticker mt-2">เจ้าถิ่น</div>
									</div>
									<div class="item col-6 col-sm-4 p-3 text-center">
										<div class="mx-auto ic-profile ic-sticker ic-sticker-2"></div>
										<div class="lb-sticker mt-2">ลอยลำ</div>
									</div>
									<div class="item col-6 col-sm-4 p-3 text-center">
										<div class="mx-auto ic-profile ic-sticker ic-sticker-3"></div>
										<div class="lb-sticker mt-2">เวลายอดเยี่ยม</div>
									</div>
									<div class="item col-6 col-sm-4 p-3 text-center">
										<div class="mx-auto ic-profile ic-sticker ic-sticker-4"></div>
										<div class="lb-sticker mt-2">เวลายอดเยี่ยม</div>
									</div>
									<div class="item col-6 col-sm-4 p-3 text-center active">
										<div class="mx-auto ic-profile ic-sticker ic-sticker-5"></div>
										<div class="lb-sticker mt-2">พิชิตคอร์ส 2 ดาว</div>
									</div>
									<div class="item col-6 col-sm-4 p-3 text-center active">
										<div class="mx-auto ic-profile ic-sticker ic-sticker-6"></div>
										<div class="lb-sticker mt-2">พิชิตคอร์ส 1 ดาว</div>
									</div>
									<div class="item col-6 col-sm-4 p-3 text-center">
										<div class="mx-auto ic-profile ic-sticker ic-sticker-7"></div>
										<div class="lb-sticker mt-2">มือฉมัง</div>
									</div>
									<div class="item col-6 col-sm-4 p-3 text-center">
										<div class="mx-auto ic-profile ic-sticker ic-sticker-8"></div>
										<div class="lb-sticker mt-2">นักตอบตัวยง</div>
									</div>
									<div class="item col-6 col-sm-4 p-3 text-center">
										<div class="mx-auto ic-profile ic-sticker ic-sticker-9"></div>
										<div class="lb-sticker mt-2">เปิดประเด็น</div>
									</div>
									<div class="item col-6 col-sm-4 p-3 text-center">
										<div class="mx-auto ic-profile ic-sticker ic-sticker-10"></div>
										<div class="lb-sticker mt-2">ผู้รอบรู้</div>
									</div>
								</div>
							</div>
						</div>
						<div class="ct-ctrl ct-wishlist">
							<h2>คอร์สที่คุณสนใจ</h2>
							<div class="ls-wishlist">
								<div class="ls-wishlist-inn card-deck">
									<div class="item card text-center p-3 pt-4">
										<button class="btn-delete" type="button"><i class="fas fa-times"></i></button>
										<div class="item-inn">
											<h3 class="lb-wishlist mb-0">โภชนาการของเด็กโต</h3>
											<h4 class="lb-wishlist2 mb-0">คอร์สทั่วไป บทที่ 8/8</h4>
											<div class="bx-img mt-2">
												<img class="img-fluid" src="images/profile/img_wishlist.png" alt="wishlist 1">
											</div>
											<div class="dt mt-3">เรื่อง : หลักการคำนวณโภชนาการ<br>ของเด็กโต 5 - 10 ขวบ</div>
											<div class="bx-btn mt-3">
												<button type="button">เริ่มบทเรียน <i class="fas fa-angle-right"></i></button>
											</div>
										</div>
									</div>
									<div class="item card text-center p-3  pt-4">
										<button class="btn-delete" type="button"><i class="fas fa-times"></i></button>
										<div class="item-inn">
											<h3 class="lb-wishlist mb-0">โภชนาการของเด็กโต</h3>
											<h4 class="lb-wishlist2 mb-0">คอร์สทั่วไป บทที่ 8/8</h4>
											<div class="bx-img mt-2">
												<img class="img-fluid" src="images/profile/img_wishlist.png" alt="wishlist 1">
											</div>
											<div class="dt mt-3">เรื่อง : หลักการคำนวณโภชนาการ<br>ของเด็กโต 5 - 10 ขวบ</div>
											<div class="bx-btn mt-3">
												<button type="button">เริ่มบทเรียน <i class="fas fa-angle-right"></i></button>
											</div>
										</div>
									</div>
									<div class="item card text-center p-3 pt-4">
										<button class="btn-delete" type="button"><i class="fas fa-times"></i></button>
										<div class="item-inn">
											<h3 class="lb-wishlist mb-0">โภชนาการของเด็กโต</h3>
											<h4 class="lb-wishlist2 mb-0">คอร์สทั่วไป บทที่ 8/8</h4>
											<div class="bx-img mt-2">
												<img class="img-fluid" src="images/profile/img_wishlist.png" alt="wishlist 1">
											</div>
											<div class="dt mt-3">เรื่อง : หลักการคำนวณโภชนาการ<br>ของเด็กโต 5 - 10 ขวบ</div>
											<div class="bx-btn mt-3">
												<button type="button">เริ่มบทเรียน <i class="fas fa-angle-right"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include 'layout/ads.php' ?>
		<?php include 'layout/footer.php' ?>
	
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
		<script src="js/jQuery.mmenu-master/dist/jquery.mmenu.js"></script>
		<script src="js/header_footer.js?t=<?php echo time(); ?>" type="text/javascript"></script>
	</body>
</html>

